#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Mar 23 15:52:54 2018

@author: wladyslawsurala
"""

import pandas as pd
from matplotlib import pyplot as plt

train_file_path = 'train.csv'
train_data = pd.read_csv(train_file_path, sep=',')

#print(train_data.columns)
#%%
print(train_data.head(3))
#%%
print(train_data.describe()["teacher_number_of_previously_posted_projects"])
#%%

plt.hist(train_data["teacher_number_of_previously_posted_projects"], bins=[0,10,450])
plt.xticks(range(0, 500, 50))
plt.show

#%%

import numpy as np
import sklearn.metrics as metrics
#%%